import React, { useState, useEffect } from "react";

export const NewApp = () => {
  const [count, updateCount] = useState(0);
  const [countObj, updateCountObj] = useState({ count: 0 });
  const [numbers, updateNumbers] = useState([]);

  useEffect(() => {
    // Component Did Mount

    const timerInterval = setInterval(() => {
      updateCount((state) => state + 1);
    }, 1000);

    return () => {
      // componentWillUnmount
      // lets clean up our memory leak ;)
      clearInterval(timerInterval);
      console.log("cleanup!");
    };
  }, []);

  useEffect(() => {
    console.log("Im bored of this demo please stop vince!");
  }, [count]);

  const increaseCounter = () => {
    updateCount((state) => state + 1);
    updateNumbers((state) => [...state, state.length + 1]);
  };

  const decreaseCounter = () => {
    updateCount((state) => state - 1);
  };

  // Counter as an object
  const increaseObjCounter = () => {
    updateCountObj((state) => ({ ...state, count: state.count + 1 }));
  };

  const decreaseObjCounter = () => {
    updateCountObj((state) => ({ ...state, count: state.count - 1 }));
  };

  if (count > 5) {
    return <h1>Count is too high!</h1>;
  }

  return (
    <>
      <h1>{count}</h1>
      <button onClick={increaseCounter}>+</button>
      <button onClick={decreaseCounter}>-</button>
      <hr />
      <h1>{countObj.count}</h1>
      <button onClick={increaseObjCounter}>+</button>
      <button onClick={decreaseObjCounter}>-</button>
      <hr />
      <ul>
        {numbers.map((num) => (
          <li key={num}>{num}</li>
        ))}
      </ul>
    </>
  );
};
