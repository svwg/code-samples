import React from "react";

class PeopleForm extends React.Component {
  state = {
    firstName: "",
    lastName: "",
    gender: "female",
  };

  handleChange = (ev) => {
    ev.persist();
    this.setState((state) => ({
      [ev.target.name]: ev.target.value,
    }));
  };

  handleSubmit = (ev) => {
    ev.preventDefault();
    this.props.addUser({ ...this.state });
    this.setState(() => ({
      firstName: "",
      lastName: "",
      gender: "female",
    }));
    this.props.history.push("/users");
  };

  render() {
    const { firstName, lastName, gender } = this.state;
    return (
      <>
        {firstName && lastName && <h5>{`${firstName} ${lastName} and I am a ${gender}`}</h5>}
        <form onSubmit={this.handleSubmit}>
          <input name="firstName" type="text" value={firstName} onChange={this.handleChange} />
          <input name="lastName" type="text" value={lastName} onChange={this.handleChange} />
          <select name="gender" value={gender} onChange={this.handleChange}>
            <option value="male">Male</option>
            <option value="female">Female</option>
          </select>
          <button type="submit">Submit</button>
        </form>
      </>
    );
  }
}

export default PeopleForm;
