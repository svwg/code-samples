import React from "react";
import {
  Link,
  Switch,
  Route,
  useRouteMatch,
  useParams,
  Redirect,
} from "react-router-dom";

const User = ({ users }) => {
  const params = useParams();
  const user = users.find(
    (user) => user.id === Number.parseFloat(params.userId)
  );
  if (!user) {
    return <Redirect to="/users" />;
  }
  return `Hi I'm ${user.firstName}, my last name is ${user.lastName} and I am a ${user.gender}`;
};

const Users = (props) => {
  const match = useRouteMatch();
  const { users } = props;
  return (
    <div>
      <h2>Users</h2>

      <ul>
        {users.map((user, idx) => (
          <li key={`user-list${user.id}`}>
            <Link
              to={`${match.url}/${user.id}`}
            >{`${user.firstName} ${user.lastName}`}</Link>
          </li>
        ))}
      </ul>

      {/* The User page has its own <Switch> with more routes
          that build on the /User URL path. You can think of the
          2nd <Route> here as an "index" page for all User, or
          the page that is shown when no topic is selected */}
      <Switch>
        <Route exact path={match.path}>
          <h3>Please select a user.</h3>
        </Route>
        <Route path={`${match.path}/:userId`}>
          <User users={users} />
        </Route>
      </Switch>
    </div>
  );
};

export default Users;
