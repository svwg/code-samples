import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Users from "./components/Users";
import PeopleForm from "./components/PeopleForm";
import "./App.css";

const About = () => "About Page";
const Home = () => "Home Page";

// const someProps = {
//   first: "Vince",
//   last: "St",
// };
// <Component first={someProps.first} last={someProps.last} />
// <Component {...someProps} first="Billy" email="Billy" />

class App extends React.Component {
  state = {
    users: [],
  };

  addUser = (user) => {
    this.setState((state) => ({
      ...state,
      users: [
        ...state.users,
        {
          ...user,
          id: Math.random() * 1000,
        },
      ],
    }));
  };

  render() {
    const { users } = this.state;
    return (
      <Router>
        <div>
          <nav>
            <ul>
              <li>
                <Link to="/">Home</Link>
              </li>
              <li>
                <Link to="/about">About</Link>
              </li>
              <li>
                <Link to="/users">Users</Link>
              </li>
              <li>
                <Link to="/people-form">People Form</Link>
              </li>
            </ul>
          </nav>

          {/* <button onClick={this.messWithUsers}>Click me</button> */}

          {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
          <Switch>
            {/* We used to do this a lot back in my day */}
            <Route exact path="/" component={Home} />
            <Route
              path="/people-form"
              render={(renderProps) => <PeopleForm {...renderProps} addUser={this.addUser} />}
            />
            <Route path="/about" render={(renderProps) => <About {...renderProps} />} />

            <Route path="/users">
              <Users users={users} />
            </Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
