import { enhancer } from "./enhancer";
import { Counter } from "./Counter";

// enhance the Counter component to actually give it the new props
// I want to export an enhanced counter and not my dumb one in Counter.js
const EnhancedCounter = enhancer(Counter);
export default EnhancedCounter;
