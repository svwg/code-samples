import React from "react";
import "./App.css";
import Counter from "./counter/index";
import { Counter as DumbCounter } from "./counter/Counter";

class App extends React.PureComponent {
  static defaultProps = {
    name: "Vince",
  };

  render() {
    return (
      <div className="App">
        <h1>Hello {this.props.name}</h1>
        <Counter />
        <DumbCounter
          count={3}
          people={[{ id: 1, name: "Vince", email: "vince@gmail.com" }]}
        />
        {/* <Counter multiplyBy={10} />
        <Counter multiplyBy={2} />
        <Counter multiplyBy={3.14159265458} /> */}
      </div>
    );
  }
}

// App.defaultProps = {
//   name: "Vince",
// };

export default App;
