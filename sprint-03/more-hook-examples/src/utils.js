export const calculateExpensiveFunction = (a, b) => a + b;

export const delay = (ms = 1000) => new Promise((resolve, reject) => setTimeout(resolve, ms));
