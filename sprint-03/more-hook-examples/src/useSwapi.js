import { useState, useEffect } from "react";

export default function usePeople() {
  const [people, updatePeople] = useState([]);

  useEffect(() => {
    async function getPeople() {
      try {
        const res = await fetch("http://swapi.dev/api/people");
        const data = await res.json();
        updatePeople(data.results);
      } catch (err) {
        console.log("Unable to fetch people from SWAPI", err);
      }
    }
    getPeople();
  }, []);

  return { people };
}
