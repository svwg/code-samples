import React, { useRef, useState } from "react";
import { useGeolocation, useDebounce } from "react-use";
import { getWeather, BBPromise, delay } from "./util";

export const WeatherLocation = () => {
  const [loading, setLoading] = useState(false);
  const [result, setResult] = useState(null);
  const locationData = useGeolocation();
  const cancellationRef = useRef(null);

  useDebounce(fetchData, 1000, [locationData.latitude, locationData.longitude]);

  function fetchData() {
    setResult(null);
    setLoading(true);
    cancelAPICalls();
    cancellationRef.current = BBPromise.resolve()
      .then(() => delay(1000))
      .then(() => getWeather(locationData))
      .then((res) => {
        setResult(res);
      })
      .catch((err) => {
        console.log("Unable to get weather", err);
      })
      .finally(() => {
        setLoading(false);
      });
  }

  function cancelAPICalls() {
    if (cancellationRef.current && !cancellationRef.current.isCancelled()) {
      cancellationRef.current.cancel();
    }
  }

  if (loading || locationData.loading) {
    return (
      <>
        <p>Loading...</p>
        <button onClick={fetchData}>Refetch</button>
        <button onClick={cancelAPICalls}>Cancel!</button>
      </>
    );
  }

  return (
    <>
      {result && <p>{JSON.stringify(result)}</p>}
      <button onClick={fetchData}>Refetch</button>
    </>
  );
};

export default WeatherLocation;
