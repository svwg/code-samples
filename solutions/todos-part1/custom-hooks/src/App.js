import React, {useCallback, useState} from "react";
import "./index.css";
import todosList from "./todos.json";


const useTodoArray = initialArray => {
    const [todos, setTodos] = useState(initialArray);

    return {
        value: todos,
        setValue: setTodos,
        add: useCallback(newTodo =>
            setTodos(currentArray => [...currentArray, newTodo])),
        deleteCompleted: useCallback(() =>
            setTodos(currentArray => currentArray.filter(todo => !todo.completed))),
        deleteById: useCallback(
            id => setTodos(
                currentArray => currentArray.filter(todo => todo && todo.id !== id))),
        toggleCompletedById: useCallback(
            id => setTodos(
                currentArray => currentArray.map(
                    todo => {
                        if (todo.id === id) {
                            return {
                                ...todo, completed: !todo.completed
                            }
                        }
                        return todo
                    }
                )
            )
        )
    }
}

const useInputField = (initialString) => {
    const [inputValue, setInputValue] = useState(initialString);

    return {
        value: inputValue,
        setValue: setInputValue,
        change: useCallback(
            event => setInputValue(event.target.value)),
        clear: useCallback(
            () => setInputValue(""))
    }
}


function App() {
    const todos = useTodoArray(todosList)
    const inputField = useInputField("")

    const handleCreate = event => {
        if (event.key === "Enter") {
            todos.add({
                userId: 1, id: Math.ceil(Math.random() * 1000000), title: inputField.value, completed: false
            });
            inputField.clear();
        }
    };

    return (<section className="todoapp">
            <header className="header">
                <h1>todos</h1>
                <input
                    className="new-todo"
                    placeholder="What needs to be done?"
                    autoFocus
                    onChange={inputField.change}
                    value={inputField.value}
                    onKeyDown={handleCreate}
                />
            </header>

            <TodoList
                todos={todos.value}
                handleToggleComplete={todos.toggleCompletedById}
                handleDelete={todos.deleteById}
            />
            <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
                <button
                    className="clear-completed"
                    onClick={todos.deleteCompleted}
                >
                    Clear completed
                </button>
            </footer>
        </section>);
}

function TodoItem(props) {
    return (<li className={props.completed ? "completed" : ""}>
        <div className="view">
            <input
                className="toggle"
                type="checkbox"
                defaultChecked={props.completed}
                onClick={props.handleToggleComplete}
            />
            <label>{props.title}</label>
            <button className="destroy" onClick={props.handleDelete}/>
        </div>
    </li>);
}

function TodoList(props) {
    return (<section className="main">
        <ul className="todo-list">
            {props.todos.map(todo => (<TodoItem
                key={todo.id}
                title={todo.title}
                completed={todo.completed}
                handleToggleComplete={() => props.handleToggleComplete(todo.id)}
                handleDelete={() => props.handleDelete(todo.id)}
            />))}
        </ul>
    </section>);
}

export default App;
