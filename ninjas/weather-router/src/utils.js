import axios from "axios";
import * as env from "./env";

export async function getWeather(locationData) {
  // console.log(`
  //   Retrieving the weather from ${env.WEATHER_API_URL}
  //   with a key of ${env.WEATHER_API_KEY}
  // `);

  if (!locationData) {
    return null;
  }

  const { data } = await axios.get(`${env.WEATHER_API_URL}/current.json`, {
    params: {
      key: env.WEATHER_API_KEY,
      q: `${locationData.latitude},${locationData.longitude}`,
    },
  });

  return data;
}
